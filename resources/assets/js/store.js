import { getLocalUser } from "./ckcmHelpers/auth";
import { getLocalfdetails } from "./ckcmHelpers/auth";
import Axios from "axios";
const user = getLocalUser();
const fdetails = getLocalfdetails();
export default {
   state: {
      accountLoginData: user,
      fdetails: fdetails,
      isIn: !!user,
      isFB: !!fdetails,
      loading: false,
      auth_error: null,
      alertLogoutDone: false,
      windowSize: [],
      leftnavDrawer: true,
   },
   getters: {
      isLoading(state) {
         return state.loading;
      },
      isIn(state) {
         return state.isIn;
      },
      isFB(state) {
         return state.isFB;
      },
      accountLoginData(state) {
         return state.accountLoginData;
      },
      authError(state) {
         return state.auth_error;
      },
      fdetails(state) {
         return state.fdetails;
      },
      alertLogoutDone(state) {
         return state.alertLogoutDone;
      },
      leftnavDrawer(state) {
         return state.leftnavDrawer
      }      
      
   },
   mutations: {
      firebaseSuccess(state, fdetailsload) {
         state.fdetails = Object.assign({},{fdetailsload} );
         localStorage.setItem("fdetails", JSON.stringify(state.fdetails));
      },
      loginSuccess(state, payload) {
         state.alertLogoutDone = false
         state.auth_error = null;
         state.isIn = true;
         state.loading = false;
         state.accountLoginData = Object.assign({}, payload, {
               token: payload.access_token
         });
         localStorage.setItem("user", JSON.stringify(state.accountLoginData));
         // localStorage.user = JSON.stringify(state.accountLoginData);
      },
      logout(state) {
         localStorage.removeItem("user");
         localStorage.removeItem("fdetails");
         state.isIn = false;
         state.fdetails = null ;
         state.accountLoginData = null;
         state.alertLogoutDone = true
         firebase.auth().signOut().then(function() {
         // Sign-out successful.
         }).catch(function(error) {
         // An error happened.
         });
      },
      jieLoaderOn(state) {
         state.loading = true;
      },
      jieLoaderOff(state) {
         state.loading = false;
      },
      alertLogoutDone(state) {
         state.alertLogoutDone = false;
      },
      leftnavDrawerOff(state) {
         state.leftnavDrawer = false;
      },
      leftnavDrawerOn(state) {
         state.leftnavDrawer = true;
      }
   },
   actions: {
      alertLogoutDone(context) {
         context.commit("alertLogoutDone");
      },
      jieLoaderOn(context) {
         context.commit("jieLoaderOn");
      },
      jieLoaderOff(context) {
         context.commit("jieLoaderOff");
      },
      loginFirebase(context) {
         const user = firebase.auth().currentUser;
         context.commit("firebaseSuccess", user)
      },
   }
}
